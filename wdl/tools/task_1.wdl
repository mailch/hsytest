version 1.0

task hello_world {
    input {
        String work_dir
        String bin_dir
        String script_dir
    }

    command<<<
    set -eo pipefail > /dev/null
    echo "hello world"
    >>>

    output{
        String flag="OK"
    }
    runtime {
        backend: 'Local'
    }
}
