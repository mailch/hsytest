version 1.0

import 'tools/task_1.wdl' as task_1

workflow main {
    input {
        String bin_dir
        String script_dir
        String work_dir

    }

    call task_1.hello_world as hello_world {
        input:
            work_dir=work_dir,
            bin_dir=bin_dir,
            script_dir=script_dir
    }

    output {
        String flag=hello_world.flag
    }
}