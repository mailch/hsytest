version 1.0

workflow main {
    input {
        String bin_dir
        String script_dir
        String work_dir

    }

    call hello_world {
        input:
            work_dir=work_dir,
            bin_dir=bin_dir,
            script_dir=script_dir
    }

    output {
        String flag=hello_world.flag
    }
}


task hello_world {
    input {
        String work_dir
        String bin_dir
        String script_dir
    }

    command<<<
    set -eo pipefail > /dev/null
    echo "hello world"
    >>>

    output{
        String flag="OK"
    }
    runtime {
        backend: 'Local'
    }
}